#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#define NUM_HILOS 2
void *Impr_Msg(void *mensaje)
{
	sleep(5);
	printf("%s \n\n", (char *)mensaje);
	pthread_exit(0);
}
int main(int argc, char *argv[])
{
	pthread_attr_t attr;
	pthread_t nhilos[NUM_HILOS];
	char *msgs[] = {"hola", "adios"};
	int i;
	pthread_attr_init(&attr);
	pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);
	for(i=0; i<NUM_HILOS; i++)
	{
		printf("%d\n", i);
		if(pthread_create(&nhilos[i], &attr, Impr_Msg, (void*) msgs[i]))
		{
			perror("Error en creacion de hilo\n	");
			exit(-1);
		}
	}
	for(i=0; i< NUM_HILOS; i++)
		pthread_join(nhilos[i], NULL);
	exit(0);
	
	return 0;
}
